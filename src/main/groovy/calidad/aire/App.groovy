/*
 * This Groovy source file was generated by the Gradle 'init' task.
 */
package calidad.aire

class App {
    String getGreeting() {
        return 'Hello world.'
    }

    static void main(String[] args) {
        println new App().greeting
    }
}
