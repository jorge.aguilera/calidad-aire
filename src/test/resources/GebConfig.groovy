/*
	This is the Geb configuration file.
	
	See: http://www.gebish.org/manual/current/#configuration
*/


import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.firefox.FirefoxProfile

waiting {
	timeout = 40
}

environments {
	
	// run via “./gradlew chromeTest”
	// See: http://code.google.com/p/selenium/wiki/ChromeDriver
	chrome {
		atCheckWaiting = 30
		driver = {
			ChromeOptions options = new ChromeOptions()
			def driverInstance = new ChromeDriver()
			driverInstance.manage().window().maximize()
			driverInstance
		}
	}

	// run via “./gradlew chromeHeadlessTest”
	// See: http://code.google.com/p/selenium/wiki/ChromeDriver
	chromeHeadless {
		driver = {
			ChromeOptions o = new ChromeOptions()
			o.addArguments('headless', 'window-size=1920,1080')
			def driverInstance = new ChromeDriver(o)
			driverInstance.manage().window().maximize()
			driverInstance
		}
	}
	
	// run via “./gradlew firefoxTest”
	// See: http://code.google.com/p/selenium/wiki/FirefoxDriver
	firefox {
		atCheckWaiting = 30

		driver = {
			FirefoxProfile profile = new FirefoxProfile()
			FirefoxOptions firefoxOptions = new FirefoxOptions(profile: profile)
			def driverInstance = new FirefoxDriver(firefoxOptions)
			driverInstance.manage().window().maximize()
			driverInstance
		}
	}

}
