package calidad.aire.geb

import geb.Page
import net.bytebuddy.implementation.bytecode.Throw

class Twitter extends Page{

    static url = "https://twitter.com/"

    void login(){
        $("input", name:"session[username_or_email]").first() << System.getProperty("twitter_user")
        $("input", name:"session[password]").first() << System.getProperty("twitter_password")
        $("input", type:"submit").first().click()
    }

    void sendTweet(String text, String file=null){
        $("div", class:"RichEditor").first().click()
        interact {
            sendKeys text
        }
        if( file ){
            $("form", action:"//upload.twitter.com/i/tweet/create_with_media.iframe").media_empty = file
        }
        if( System.getProperty("twitter_send")== null || System.getProperty("twitter_send") == 'true')
            $("button", class:"tweet-action", type:"button").first().click()
        sleep(5*1000) // dar tiempo a la subido
    }
}
