package calidad.aire.geb

import geb.Page
import org.openqa.selenium.By
import org.openqa.selenium.OutputType
import org.openqa.selenium.Point
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebElement

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

class Aqicn extends Page{

    String getPais(){
        System.getProperty("pais") ?: "spain"
    }

    String getProvincia(){
        System.getProperty("provincia") ?: "madrid"
    }

    String getCiudad(){
        System.getProperty("ciudad") ?: "madrid"
    }

    String getIdioma(){
        System.getProperty("idioma") ?: "es"
    }


    @Override
    String getPageUrl(String path) {
        String url ="http://aqicn.org/city/$pais/$provincia/$ciudad/$idioma/"
        url
    }

    static at = { $("#h1header2").text().toUpperCase().indexOf(provincia.toUpperCase()) != -1}

    BufferedImage extractDiv(){
        WebElement ele = driver.findElement(By.id("citydivmain"))

        File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        BufferedImage fullImg = ImageIO.read(screenshot);
        ImageIO.write(fullImg,"png", new File("build/fullscreen.png") )

        Point point = ele.location
        int eleWidth = ele.size.width
        int eleHeight = ele.size.height

        BufferedImage eleScreenshot= fullImg.getSubimage(point.x, point.y,eleWidth, eleHeight)
        eleScreenshot
    }

}
